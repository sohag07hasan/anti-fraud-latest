<style>
	div.payment_details{
		float: left;
		margin-right: 20px;
			
	}
	
	div.order_data_column_container_antifraud  p{
		color: #777;
	}
	
	div.ip_details{
		float: right;
		margin-right: 100px;		
	}
	
	div.fraud_status{
		font-size: 1.5em;
		font-wight: bold;
		width: 100%;
		
	}
	
	div.fraud_status p{
		margin-left: 20px;
	}
	
	div.verified{
		border: .2em solid #1FD91F;
		background: #00FF00;
	}
	
	div.not-verified{
		border: .2em solid #4D4D4D;
		background: #BFBFBF;		
	}
	
	div.fraud{
		border: .2em solid #A52A2A;
		background: #FF0000;		
	}
	
	div.woocommerce farud-order-management p.status-showing{		
		color: #1E90FF;
		font-size: 14px;		
	}
	
	div.soft-fraud{
		background: #FFA500;
		border: .2em solid #D0A12D;
	}
	
</style>


<?php 
	global $post, $wpdb, $thepostid, $theorder, $order_status, $woocommerce;
	$thepostid = absint( $post->ID );

	if ( ! is_object( $theorder ) )
		$theorder = new WC_Order( $thepostid );

	$order = $theorder;
	
	$available_gateways = $woocommerce->payment_gateways->get_available_payment_gateways();
	//var_dump($available_gateways);
	
?>
<div class="woocommerce farud-order-management">
	
	<?php		
		$db = self::get_db_instace();
		$customer = $db->get_customer_by('email', $order->billing_email);
		
		/*
		var_dump($customer);
		
		//trying to get customer by current loggedin user
		if(empty($customer) && is_user_logged_in()){
			$current_user = wp_get_current_user();
				
			//trying to get customer by current user username
			$customer = $db->get_customer_by('username', $current_user->user_login);
				
			//trying to get customer by current user email
			if(empty($customer)){
				$customer = $db->get_customer_by('email', $current_user->user_email);
			}
		}
		*/
		
	?>	
	
	<?php if($customer): ?>
		
		<?php if($customer->status == 'ok'):?>
			
			<div class="fraud_status verified">
				<p> <a style="text-decoration:none;" href="<?php echo admin_url('admin.php?page=anti-fraud-management&customer_id=' . $customer->ID); ?>"> Verified </a> </p>
			</div>
		
		<?php elseif($customer->status == 'hard'): ?>
		
			<div class="fraud_status fraud">
				<p> <a style="text-decoration:none;" href="<?php echo admin_url('admin.php?page=anti-fraud-management&customer_id=' . $customer->ID); ?>"> Blacklisted </a> </p>
			</div>
		<?php else: ?>
			
			<div class="fraud_status soft-fraud">
				<p> <a style="text-decoration:none;" href="<?php echo admin_url('admin.php?page=anti-fraud-management&customer_id=' . $customer->ID); ?>"> Soft Blacklisted </a> </p>
			</div>
			
		<?php endif; ?>
		
	<?php else: ?>
		<div class="fraud_status not-verified">
			<p> <a style="text-decoration:none;" href="<?php echo admin_url('admin.php?page=anti-fraud-management&order_id=' . $order->id); ?>"> UNVERIFIED </a> </p>
		</div>
	<?php endif;?>
	
			
		<div class="order_data_column_container_antifraud">
			
		<?php 
			if($order->payment_method == 'paypal'){
				$payer_name = get_post_meta($post->ID, 'Payer first name', true) . ' ' . get_post_meta($post->ID, 'Payer last name', true);
				$billing_name = $order->billing_first_name . ' ' . $order->billing_last_name;
			
				$payer_email = get_post_meta($post->ID, 'Payer PayPal address', true);
			?>
				
				<div class="payment_details">
					<h4>Payment: <?php echo $available_gateways[$order->payment_method]->title; ?></h4>
					<p><strong>Paypal Name: </strong><?php echo $payer_name;	?></p>
					<p><strong>Paypal Email: </strong><?php echo $payer_email; ?></p>
					<p>
						<strong>Status (Billing address) :</strong><br/>
						
						<?php echo strtolower($payer_name) == strtolower($billing_name) ? 'Matched' : 'Not Matched'; ?>
											
					</p>
					
					<p>
						<strong>Status (Paypal Email) :</strong><br/>
						
						<?php echo strtolower($payer_email) == strtolower($order->billing_email) ? 'Matched' : 'Not Matched' ?>
											
					</p>
										
				</div>
				
				<?php 
					}
					elseif($order->payment_method == 'stripe'){
						$stripe_info = get_post_meta($order->id, '_customer_stripe_info', true);
						?>
						
						<div class="payment_details">
						<h4>Payment: <?php echo $available_gateways[$order->payment_method]->title; ?></h4>
						<p> Attempted Transaction: <?php echo count($stripe_info); ?> </p>
						<?php 
							if($stripe_info){
								
							//	var_dump($stripe_info);
								
								foreach($stripe_info as $si){
									?>
									
										<h4>Card Info</h4>
										<p><strong> Card Number: </strong> .... <?php echo $si['active_card']; ?> </p>
										<p><strong> Exp Year : </strong> <?php echo $si['exp_year']; ?> </p>
										<p><strong> Exp Month : </strong> <?php echo $si['exp_month']; ?> </p> 
									
									
									<?php 
								}
							}
						?>
						
					</div>
					
						
						<?php 						
					}
					else{
						?>
						<div class="payment_details">
							<h4>Payment: <?php echo $available_gateways[$order->payment_method]->title; ?></h4>
						</div>
						<?php 
					}

				?>
				
				<div class="ip_details">
					<h4>IP Details</h4>
					<?php $ip = get_post_meta($order->id, '_customer_ip_address', true);  ?>
					<p>
						<strong>IP: </strong><?php echo $ip; ?>
					</p>
					
					<p class="status-showing">
						<strong> IP Status:</strong>
						<?php 							
							$ip_status = array();
							if($db->is_suspicious('ip', $ip)){
								$ip_status[] = 'IP address is suspicious';
							}
							if($ip_range = $db->is_between_suspecious_ip_range($ip)){
								$ip_status[] = "IP Range ( $ip_range->start to $ip_range->end ) is suspicious";
							}
							
							if(!empty($ip_status)){
								echo implode(', ', $ip_status);
							}
							else{
								echo 'Ip is valid';
							}
						?>					
					</p>
					
					<?php $country = get_post_meta($order->id, '_ip_country', true); ?>
					<p>
						<strong>Country: </strong>  <?php echo $country; ?>
					</p>
					<p class="status-showing">
						<strong>Country Status:</strong>
						<?php 
							 if($db->is_suspicious('country', $country)){
							 	echo "$country is in suspicious lists";
							 }
							 else{
							 	echo "$country is valid";
							 }
						?>
					</p>
					<p>
						<strong>State: </strong>  <?php echo get_post_meta($order->id, '_ip_state', true); ?>
					</p>
					
					<?php $city = get_post_meta($order->id, '_ip_city', true); ?>
					
					<p>
						<strong>City: </strong>  <?php echo $city; ?>						
					</p>
					
					<p class="status-showing">
						<strong>City Status</strong>
						<?php  
							if($db->is_suspicious('city', $city)){
								echo "$city is in suspicious lists";
							}
							else{
								echo "$city is valid";
							}
						?>
					</p>
					
					<p>
						<strong>ISP: </strong>  <?php echo get_post_meta($order->id, '_ip_isp', true); ?>
					</p>
					<p>
						<strong>Timezone: </strong>  <?php echo get_post_meta($order->id, '_ip_time_zone', true); ?>
					</p>
					
					<p>
						<strong>Proxy Used?: </strong>  <?php echo (get_post_meta($order->id, '_ip_is_proxy', true)) ? 'Yes' : 'No'; ?>
					</p>
					
				</div>
												
				<div style="clear:both"></div>
			
			</div>
			
	</div>