<?php
	$message = self::woocommerce_hard_blacklist_message();	
?>

<h3> Blacklist Messages </h3>


<table class="form-table">
	<tbody>
		
		
		<tr valign="top">
			<th scope="row" class="titledesc"><label for="woocommerce_hard_blacklist_message"> Message for hard blacklist </label></th>
			<td class="forminp forminp-checkbox">
				<fieldset>
					<legend class="screen-reader-text"><span> Options (one per line) </span></legend>
					<textarea rows="5" cols="80" id="woocommerce_hard_blacklist_message" name="woocommerce_hard_blacklist_message" ><?php echo $message; ?></textarea>										
				</fieldset>
			</td>
		</tr>
		
		
	</tbody>
</table>