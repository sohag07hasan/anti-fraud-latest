<style>
	.instruction{
		font-size: 15px;
	}
	
	.blacklist{
		color: red;
	}
	
	td.blacklisted{
		font-weight: bold;
	}
	
	div.tab form td{
		font-size: 15px;
	}
	
</style>

<?php 
	
	$page_url = admin_url('admin.php?page=anti-fraud-management');
	$tabs = array(
		array(
			'slug' => 'verified',
			'display' => 'Verified Customers'
		),	
		array(
			'slug' => 'blacklisted',
			'display' => 'Blacklisted Customers'
		),
		array(
			'slug' => 'soft-blacklisted',
			'display' => 'Soft Blacklisted Customers'
		)
	);
	
?>

<div class="wrap">
	<?php screen_icon('tools'); ?>
	<h2 class="nav-tab-wrapper">
		<?php 
			foreach($tabs as $tab){
				if(isset($_GET['tab']) && $_GET['tab'] == $tab['slug']){
					$class = 'nav-tab-active nav-tab';
				}
				elseif(empty($_GET['tab']) && $tab['slug'] == 'verified'){
					$class = 'nav-tab-active nav-tab';
				}
				else{
					$class = 'nav-tab';
				}
				?>
				<a class="<?php echo $class; ?>" href="<?php echo self::get_tab_url($tab); ?>"><?php echo $tab['display']; ?></a>
				<?php 
			}
		?>
	</h2>
	
	<?php 
		include self::get_appropirate_tab();
	?>
	
</div>