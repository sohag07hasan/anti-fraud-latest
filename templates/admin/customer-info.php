<style>
	div.information{
		margin: 0px 0px 50px 0px;
	}
	
	h3{
		margin-bottom: 2px; 
	}
	
	.information select{
		width: 150px;
		height: 35px;
		text-align: center;
	}
	
</style>

<?php 
	
	$list = self::$suspecious_list;
	$db = self::get_db_instace(); // $db = new WooAntiFraudDb();
	$customer = array();

	if(is_multisite()){
		$status = $db->check_if_tables_installed();
		if(false === $status){
			$db->sync_db();
		}
	}
	
	
	if($_GET['order_id'] > 0){
		$customer = self::get_customer_info_by_order($_GET['order_id']);
		$is_order = true;
		//checking at customer table
		$customer_id =	get_post_meta($order->id, '_verified_customer_id', true);		
		$db_customer = $db->get_customer_by('ID', $customer_id);
		
		//additional info
		$note = $db->get_customer_meta($customer_id, 'note');
		$attachment = $db->get_customer_meta($customer_id, 'attachment');
		
		//status
		$status = ($db_customer) ? $db_customer->status : '';
		
		
	}
	elseif($_GET['customer_id'] > 0){
		$customer_id = trim($_GET['customer_id']);
		$db_customer = $db->get_customer_by('ID', $customer_id);		
		
		$customer['name'] = $db->get_customer_meta($customer_id, 'name');
		$customer['username'] = $db_customer->username;
		$customer['email'] = $db_customer->email;
		$customer['ip'] = $db_customer->ip;
				
		$customer['city'] = $db->get_customer_meta($customer_id, 'city');
		$customer['timezone'] = $db->get_customer_meta($customer_id, 'timezone');
		$customer['country'] = $db->get_customer_meta($customer_id, 'country');

		$customer = self::make_form_layout($customer);		
		//additional info
		$note = $db->get_customer_meta($customer_id, 'note');
		$attachment = $db->get_customer_meta($customer_id, 'attachment');
		
		//status
		$status = $db_customer->status;
		
		//var_dump($status);
		
	}
?>

<div class="wrap">
	<h2> Customer Info </h2>
	
	<?php if($_GET['message'] == 1): ?>
		<div class="updated"><p>Customer is saved</p></div>.
	<?php elseif($_GET['message'] == 2): ?>
		<div class="error"><p>Customer is not saved! Please try again</p></div>
	<?php endif; ?>
	
	<form action="" method="post">
	
		<input type="hidden" name="customer_profile_save" value="Y" />
		
		<?php if($_GET['order_id'] > 0){ ?>
			<input type="hidden" name="order_id" value="<?php echo $_GET['order_id']; ?>" />
		<?php } ?>
		
				
		<div class="basc_information information">
			<h3>Basic Information</h3>
			<table class="form-table">
				<tbody>				
					<?php foreach($customer as $key => $value): ?>
					<tr valign="top">
						<th scope="row"><?php echo $value['title']; ?>: </th>
						<td colspan="2"> <input size="50" type="text" readonly name="customer[<?php echo $key; ?>]" value="<?php echo $value['value']; ?>"> </td>
						<?php if(in_array($key, $list) && $value['value'] != 'Non User') : ?>
						<td> 
							<input <?php echo $db->is_black_listed($customer_id, $key, $value['value']) ? 'checked' : ''; ?> id="<?php echo $key; ?>" type="checkbox" name="checkbox[<?php echo $key; ?>]" /> <label for="<?php echo $key ?>"> Suspicious? </label> 
						</td>
						<?php else: ?>
							<td>&nbsp;</td>
						<?php endif; ?>
						
						<td>&nbsp;</td>
						
					</tr>
					<?php endforeach;?>
				</tbody>
			</table>
		</div>
		
		<div class="verification_informaiton information">
			<h3> Customer Status:  &nbsp; &nbsp; &nbsp;
				<select name="customer[status]">
					<option <?php selected('ok', $status); ?> value="ok"> Verified </option>
					<option <?php selected('hard', $status); ?> value="hard"> Blacklisted </option>
					<option <?php selected('soft', $status); ?> value="soft"> Soft Blacklisted </option>
				</select>
			</h3>
		</div>
		
		<?php 
			if($db_customer){
				$ip_ranges = $db->get_ip_ranges($customer_id);
			}
		?>
		
		<div class="iprange_information information">
			<h3> IP Ranges </h3>
			<p> You can add some suspecious ip ranges for this specific customer</p>
			<table class="form-table">
				<tr>
					<th scope="row">Range 1:</th>
					<td> <input value="<?php echo $ip_ranges->start; ?>" type="text" name="ip_range[start]">Start  </td>
					<td> <input value="<?php echo $ip_ranges->end; ?>" type="text" name="ip_range[end]"> End </td>
				</tr>
				<!-- 
				<tr>
					<th scope="row">Range 2:</th>
					<td> <input type="text" name="start_range[]"> Range 2 Start  </td>
					<td> <input type="text" name="end_range[]"> Range 2 End </td>
				</tr>
				 -->
				
			</table>					
			
		</div>
		
						
		<div class="order_history information">
			<h3> Order History </h3>
			<p> Only ten latest orders are listed here </p>
			
			<?php $orders = self::get_orders($customer_id); ?>
			<?php if(count($orders) > 0): ?>
			
				<table class="widefat">
					<thead>
						<th>Order</th>
						<th> Status </th>
						<th> Order Total </th>
						<th> Date </th>
						<th> IP </th>
					</thead>
				
					<?php foreach($orders as $o_id): ?>
						<?php $the_order = new WC_Order($o_id); ?>
						<?php $post = get_post($o_id); ?>
						
						<tr>
							<td> <?php echo '<a href="' . admin_url( 'post.php?post=' . absint( $o_id ) . '&action=edit' ) . '"><strong>' . sprintf( __( 'Order %s', 'woocommerce' ), esc_attr( $the_order->get_order_number() ) ) . '</a>' ?> </td>
							<td> <?php printf( '<mark class="%s tips" data-tip="%s">%s</mark>', sanitize_title( $the_order->status ), esc_html__( $the_order->status, 'woocommerce' ), esc_html__( $the_order->status, 'woocommerce' ) ); ?> </td>
							<td> <?php echo esc_html( strip_tags( $the_order->get_formatted_order_total() ) ); ?> </td>
							<td> 
								<?php 
									if ( '0000-00-00 00:00:00' == $post->post_date ) {
										$t_time = $h_time = __( 'Unpublished', 'woocommerce' );
									} else {
										$t_time = get_the_time( __( 'Y/m/d g:i:s A', 'woocommerce' ), $post );
									
										$gmt_time = strtotime( $post->post_date_gmt . ' UTC' );
										$time_diff = current_time('timestamp', 1) - $gmt_time;
									
										if ( $time_diff > 0 && $time_diff < 24*60*60 )
											$h_time = sprintf( __( '%s ago', 'woocommerce' ), human_time_diff( $gmt_time, current_time('timestamp', 1) ) );
										else
											$h_time = get_the_time( __( 'Y/m/d', 'woocommerce' ), $post );
									}
									
									echo '<abbr title="' . esc_attr( $t_time ) . '">' . esc_html( apply_filters( 'post_date_column_time', $h_time, $post ) ) . '</abbr>';
								?> 
							</td>
							
							<td> <?php echo get_post_meta($post->ID, '_customer_ip_address', true); ?> </td>
							
						</tr>
											
					<?php endforeach;?>
				</table>
			
			<?php else: ?>
				<strong>This customer's orders are deleted or first order</strong>			
			<?php endif; ?>
			
		</div>
		
		
		
		<div class="additional_information information">
			<h3>Additional Information</h3>
				
			<table class="form-table">
				<tr>
					<th scope="row">Attach Image/Document</th>
					<td>
									
						
						<span class='upload'>
					        <input type='text' id='wptuts_favicon' class='regular-text text-upload' name='attachment' value='<?php echo esc_url( $attachment ); ?>'/>
					        <input type='button' class='button button-upload' value='Upload Attachment/Image'/></br>
					        
					        <?php if($attachment): ?>
					        	<a class="preview-upload" href="<?php echo $attachment; ?>" target="_blank">View</a>
					        <?php endif; ?>
					        
					     </span>
						 
						<div id="customer_attachment_link_puller" style="display: none"></div>
					</td>
				</tr>
				
				
				
				<tr>
					<th scope="row">Additional Note</th>
					<td>					
						<textarea rows="5" cols="100" name="customer_additional_note"><?php echo $note; ?></textarea>
					</td>
				</tr>
			</table>			
		</div>
		
		<p><input type="submit" value="Save Customer" class="button button-primary" /></p>
		
	</form>
</div>