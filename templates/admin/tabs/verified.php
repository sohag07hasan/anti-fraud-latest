<?php 
	$list_table = self::get_list_table('ok');
	
?>

<div class="wrap">
	
	<h2>Verified Customers
		<?php 
			if(isset($_REQUEST['s']) && !empty($_REQUEST['s'])){
				echo '<span class="subtitle">Search results for “'.$_REQUEST['s'].'”</span>';
			}
		?>
	</h2>
	
	<?php 
		if($_REQUEST['deleted'] > 0) echo '<div class="updated"><p>' . $_REQUEST['deleted'] . ' deleted! </p></div>';
	?>
	
	<form action="" method="get">
		<input type="hidden" name="page" value="<?php echo $_REQUEST['page']; ?>">
		<?php
			$list_table->prepare_items();
			$list_table->search_box('Search', 'keyword');
			$list_table->display();
		?>
	</form>	
	
</div>