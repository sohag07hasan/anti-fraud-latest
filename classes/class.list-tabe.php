<?php 

/**
 * List table
 * */

if( ! class_exists( 'WP_List_Table' ) ) {
	if(!class_exists('WP_Internal_Pointers')){
		require_once( ABSPATH . '/wp-admin/includes/template.php' );
	}
	require_once( ABSPATH . '/wp-admin/includes/class-wp-list-table.php' );
}

class CustomersListTable extends WP_List_Table{
	
	private $per_page;
	private $total_items;
	private $current_page;
	public $db;
	private $type;
	
	function __construct($type = ''){
		$this->type = $type;
		$this->db = WooAntiFraudOrderPage::get_db_instace();
		parent::__construct();
	}
	
	/*preparing items must overwirte the mother function*/
	function prepare_items(){
			
		$columns = $this->get_columns();
		$hidden = array();
		$sortable = $this->get_sortable_columns();
	
		$this->_column_headers = array($columns, $hidden, $sortable);
	
		//paginations
		$this->_set_pagination_parameters();
	
		//every elements
		$this->items = $this->populate_table_data();
	}
	
	//get the column names
	function get_columns(){
		$columns = array(
				'cb' => '<input type="checkbox" />',
				'username' => __('Username'),
				'email' => __('Email'),
				'ip' => __('IP'),
				'reg_date' => __('Registered Date')
		);
	
		return $columns;
	}
	
	//make some column sortable
	function get_sortable_columns(){
		$sortable_columns = array(
				'username' => array('username', false),
				'email' => array('email', false),
				'reg_date' => array('reg_date', false)
		);
	
		return $sortable_columns;
	}
	
	
	//pagination
	private function _set_pagination_parameters(){
		//toal item measuring sql
		$table = $this->db->customer_table;
		$sql = "select count(ID) from $table where status like '$this->type'";
		if($_REQUEST['s']){
			$s = $_REQUEST['s'];
			$sql .= " and (username like '%$s%' or email like '%$s%' or ip like '%$s%')";
		}
		
		$this->current_page = $this->get_pagenum(); //it comes form mother class (WP_List_Table)
	
		$this->total_items = $this->db->db->get_var($sql);
		$this->per_page = 25;
	
		$this->set_pagination_args( array(
				'total_items' => $this->total_items,                  //WE have to calculate the total number of items
				'per_page'    => $this->per_page,                     //WE have to determine how many items to show on a page
				'total_pages' => ceil($this->total_items/$this->per_page)   //WE have to calculate the total number of pages
		) );
	
	}
	
	
	//collectes every information
	function populate_table_data(){
		
		$return = array();
		$table = $this->db->customer_table;
		$sql = "select * from $table where status like '$this->type'";	
		
		if(isset($_REQUEST['s'])){
			$s = $_REQUEST['s'];
			$sql .= " and (username like '%$s%' or email like '%$s%' or ip like '%$s%')";
		}
		
		//order
		$order_by = (isset($_GET['orderby'])) ? $_GET['orderby'] : 'reg_date';
		$order = (isset($_GET['order'])) ? $_GET['order'] : 'desc';
		$sql .= " order by $order_by $order";
		
		//pagination
		$current_page = ($this->current_page > 0) ? $this->current_page - 1 : 0;
		$offset = (int) $current_page * (int) $this->per_page;
		$sql .= " limit $this->per_page offset $offset";
		
		//var_dump($sql);
			
		$customers = $this->db->db->get_results($sql);
					
		if($customers){
			foreach($customers as $c){
				$return[] = array(
							'ID' => $c->ID,
							'email' => $c->email,
							'username' => $c->username,
							'ip' => $c->ip,
							'reg_date' => date('Y/m/d', $c->reg_date)
						);
			}
		}
		
		return $return;
	}
	
	
	
	/* Utility that are mendatory   */
	
	/* checkbox for bulk action*/
	function column_cb($item) {
		return sprintf(
				'<input type="checkbox" name="customer_id[]" value="%s" />', $item['ID']
		);
	}
	
	
	/* default column checking and it is must */
	function column_default($item, $column_name){
		switch($column_name){
			case "ID":
			case "username":
			case "email":
			case "ip":
			case "reg_date":
				return $item[$column_name];
				break;
			default:
				var_dump($item);
					
		}
	}
	
	/**
	 * extra links under every row of username column
	 * */
	function column_username($item){
		$delete_href = sprintf('?page=%s&action=%s&tab=%s&customer_id=%s', $_REQUEST['page'], 'delete', $_REQUEST['tab'], $item['ID']);
		
		if(isset($_REQUEST['s']) && !empty($_REQUEST['s'])){
			$delete_href = add_query_arg(array('s'=>$_REQUEST['s']), $delete_href);
		}
		
		if($this->get_pagenum()){
			$delete_href = add_query_arg(array('paged'=>$this->get_pagenum()), $delete_href);
		}	
		
		
		$actions = array(
				'edit' => sprintf('<a href="?page=%s&action=%s&customer_id=%s">Edit</a>', $_REQUEST['page'], 'edit', $item['ID']),
				'delete' => "<a href='$delete_href'>Delete</a>"
		);
		
		
		return sprintf('%1$s %2$s', $item['username'], $this->row_actions($actions) );
	}
	
	
	//handle bulk action
	function handle_bulk_action(){
		$message = 0;
		if($this->current_action() == 'delete'){
			$customer_ids = $_REQUEST['customer_id'];
		
			if(!is_array($customer_ids)){
				$customer_ids = array($customer_ids);
			}
				
			foreach($customer_ids as $customer_id){
				$this->db->delete_customer($customer_id);
			}
				
			$message = count($customer_ids);
		}
		
		return $message;
		
	}
	
	//bulk actions initialization
	function get_bulk_actions() {
		$actions = array(
				'delete'    => 'Delete'
		);
		return $actions;
	}
	
	
}


?>