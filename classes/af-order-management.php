<?php
/*
 * 1. This class creates a tab in order page
 * */

class WooAntiFraudOrderPage{
	
	static $suspecious_list = array('username', 'email', 'ip', 'country', 'city');
	static $default_message = 'We are unable to process your order at this time. Please contact Customer Support.';
	
	//initialize
	static function init(){

		//add a metabox in order details page
		add_action( 'add_meta_boxes', array(get_class(), 'woocommerce_meta_boxes' ));
				
		//before checkout process. Verify if a custom is hard blacklisted or not
		add_action('woocommerce_checkout_process', array(get_class(), 'verify_customer'));
				
		//order is created and identify the proxy, vpn, location order meta
		add_action('woocommerce_checkout_update_order_meta', array(get_class(), 'save_customer_identity'), 10, 2);
		
		//add_action('init', array(get_class(), 'check'), 0);
		
		add_action('admin_menu', array(get_class(), 'admin_menu'), 100);
		
		//save stripe info
		add_action('before_processing_stripe_payment', array(get_class(), 'save_customer_stripe_info'), 100, 3);
		
		//for loggedin user
		add_action('save_sripe_info_with_order', array(get_class(), 'save_stripe_response'), 100, 2);		
		
		register_activation_hook(WOOANTIFRAUD_FILE, array(get_class(), 'plugin_activated'));
		//register_deactivation_hook(WOOANTIFRAUD_FILE, array(get_class(), 'plugin_deactivated'));		
		
		//add or update customers
		add_action('admin_init', array(get_class(), 'save_customer'));		

		//list table form handler
		add_action('admin_init', array(get_class(), 'list_table_form_handler'));
		
		//js
		add_action('admin_enqueue_scripts', array(get_class(), 'admin_enqueue_scripts'));

		//new settings tab
		add_filter('woocommerce_settings_tabs_array', array(get_class(), 'add_new_settings_tab'), 90, 1);
		//populate the new tab
		add_action('woocommerce_settings_tabs_anti_fraud', array(get_class(), 'populate_new_settings_tab'));
		//save new settings
		add_action('woocommerce_update_options_anti_fraud', array(get_class(), 'save_new_settings_tab'));
			
	}
	
	
	
	/**
	 * admin enqueue scripts
	 * */
	static function admin_enqueue_scripts(){
		
		if($_GET['page'] == 'anti-fraud-management'){		
			wp_register_style('woo_anti_fraud_management_css', WOOANTIFRAUD_URL . 'staticfiles/css/woo_anti_fraud_management_css.css');
			wp_enqueue_style('woo_anti_fraud_management_css');
			
			wp_enqueue_style( 'thickbox' ); // Stylesheet used by Thickbox
   			wp_enqueue_script( 'thickbox' );
   			wp_enqueue_script( 'media-upload' );	
   					
			wp_register_script('woo_anti_fraud_management_js', WOOANTIFRAUD_URL . 'staticfiles/js/woo_anti_fraud_management_js.js', array('jquery'));
			wp_enqueue_script('woo_anti_fraud_management_js');
		}
		
	}
	
	
	
	//admin menu
	static function admin_menu(){
		add_submenu_page('woocommerce', 'Anti Fraud Campaign', 'Customers', 'manage_woocommerce', 'anti-fraud-management', array(get_class(), 'anti_fraud_management'));
		//	add_menu_page('Woocomerce Fraud Mangement', 'Woo Cutomers', 'manage_woocommerce', 'fraud-management', array(get_class(), 'anti_fraud_management'), null, 58);
		//	add_submenu_page('fraud-management', 'Add New', 'Add New', 'manage_woocommerce', 'fraud-managment-add-new', array(get_class(), 'add_new_customer'));
	}

	
	/**
	 * Anti Fraud Management
	 */
	static function anti_fraud_management(){
		if((isset($_GET['order_id']) && !empty($_GET['order_id'])) || (isset($_GET['customer_id']) && !empty($_GET['customer_id']) && $_GET['action'] != 'delete')){
			include self::get_file_dir('templates/admin/customer-info.php');
		}
		else{
			include self::get_file_dir('templates/admin/fraud-campaigns.php');
		}
	}	
	
	
	//submneu page not currently not used	
	static function add_new_customer(){
		include self::get_file_dir('templates/admin/fraud-management.php');
	}
	
	
	
	/**
	 * convenient way to check things against init hook
	 * */
	static function check(){
		$ip = '119.30.39.230';
		$IP = new WooIpInfo($ip);
		
		$info = $IP->get_ip_info();
		
		var_dump($info);
		exit;
	}
	
	
	//metabox for order page
	static function woocommerce_meta_boxes(){
		add_meta_box( 'woocommerce-order-anit-fraud', __( 'Fraud Order Mangement', 'woocommerce' ), array(get_class(), 'woocommerce_order_fraud_management'), 'shop_order', 'normal', 'high' );
	}
	
	
	//metabox holder
	static function woocommerce_order_fraud_management(){
		include self::get_file_dir('templates/metabox/woocommerce_order_fraud_management.php');
	}
	
	
	static function get_file_dir($file = ''){
		if($file){
			$file = WOOANTIFRAUD_DIR . '/' . $file;
		}
		
		return $file;
	}
	
	
	/*
	 * when an order is created it also saves customer extra identity to be used for anti fraud campaign
	 */
	static function save_customer_identity($order_id, $order_data){
		$ip = isset( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'] ;

		update_post_meta($order_id, '_customer_ip_address', $ip);
		
		$IP = new WooIpInfo($ip);
		$info = $IP->get_ip_info();		
		if($info){
			$info['is_proxy'] = isset( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ? 1 : 0;
			foreach($info as $key => $value){
				update_post_meta($order_id, '_ip_' . $key, $value);
			}
		}
		
		//add info for existing customers
		$order = new WC_Order($order_id);
		
		$db = self::get_db_instace();
		$customer = $db->get_customer_by('email', $order->billing_email);
		
		//trying to get customer by current loggedin user
		if(empty($customer) && is_user_logged_in()){
			$current_user = wp_get_current_user();
		
			//trying to get customer by current user username
			$customer = $db->get_customer_by('username', $current_user->user_login);
		
			//trying to get customer by current user email
			if(empty($customer)){
				$customer = $db->get_customer_by('email', $current_user->user_email);
			}
		}
		
		
		if($customer){
			update_post_meta($order_id, '_verified_customer_id', $customer->ID);
			$db->add_order_for_a_customer($customer->ID, 'order_id', $order_id);
		}
		
	}
	
	
	
	/*
	 * Save stripe payment info
	 * */
	static function save_customer_stripe_info($order, $stripe_token, $customer_id){
		
		if(is_user_logged_in() && $customer_id){
			$customer_ids = get_user_meta( get_current_user_id(), '_stripe_customer_id', false );
			if ( isset( $customer_ids[ $_POST['stripe_customer_id'] ]['customer_id'] ) ){
				$customer_info = $customer_ids[ $_POST['stripe_customer_id'] ];
				
				if($strip_info = get_post_meta($order->id, '_customer_stripe_info', true)){
					$strip_info[] = $customer_info;
				}
				else{
					$strip_info = array();
					$strip_info[] = $customer_info;
				}
			}
			
			update_post_meta($order->id, '_customer_stripe_info', $strip_info);
		}	
				
	}
	
	
	/*
	 * save the strip info and return the cusomter id
	 * */
	static function save_stripe_info($order, $stripe_token){
		global $woocommerce;
		$available_gateways = $woocommerce->payment_gateways->get_available_payment_gateways();
				
		$data = array(
				'email'       => $order->billing_email,
				'description' => 'Customer: ' . $order->shipping_first_name . ' ' . $order->shipping_last_name,
				'card'        => $stripe_token
			);
		
				
		//var_dump($available_gateways);
			
		$response = $available_gateways[ $woocommerce->checkout->posted['payment_method'] ]->stripe_request($data, 'customers');
		
		
		if($strip_info = get_post_meta($order->id, '_customer_stripe_info', true)){
			$strip_info[] = array(
				'customer_id' 	=> $response->id,
				'active_card' 	=> $response->active_card->last4,
				'exp_year'		=> $response->active_card->exp_year,
				'exp_month'		=> $response->active_card->exp_month,
			);
		}
		else{
			$strip_info = array();
			$strip_info[] = array(
				'customer_id' 	=> $response->id,
				'active_card' 	=> $response->active_card->last4,
				'exp_year'		=> $response->active_card->exp_year,
				'exp_month'		=> $response->active_card->exp_month,
			);
		}
		
		update_post_meta($order->id, '_customer_stripe_info',  $strip_info);
		
		return $response->id;
	}
	
	
	
	//this is for logged in users
	static function save_stripe_response($order, $response){
		
		
		if($strip_info = get_post_meta($order->id, '_customer_stripe_info', true)){
			$strip_info[] = array(
				'customer_id' 	=> $response->id,
				'active_card' 	=> $response->active_card->last4,
				'exp_year'		=> $response->active_card->exp_year,
				'exp_month'		=> $response->active_card->exp_month,
			);
		}
		else{
			$strip_info = array();
			$strip_info[] = array(
				'customer_id' 	=> $response->id,
				'active_card' 	=> $response->active_card->last4,
				'exp_year'		=> $response->active_card->exp_year,
				'exp_month'		=> $response->active_card->exp_month,
			);
		}
		
		update_post_meta($order->id, '_customer_stripe_info',  $strip_info);
	}
	
	
	/**
	 * During plugin activation period
	 * */
	static function plugin_activated(){
		$db = self::get_db_instace();
		return $db->sync_db();
	}
	
	
	//plugin deactivated
	static function plugin_deactivated(){
		$db = self::get_db_instace();
		return $db->drop_tables();
	}
	
	
	/*
	 * database intance
	 * */
	static function get_db_instace(){
		if(!class_exists('WooAntiFraudDb')){
			include self::get_file_dir('classes/class.woo-anti-fraud-db.php');
		}
		
		return new WooAntiFraudDb();
	}
	
	
	
	/**
	 * Save the customer inclugin blcalist parameter
	 * */
	static function save_customer(){
		return self::save_customer_new();		
	}
	
	
	
	/*
	 * save the customer info new
	 * */
	static function save_customer_new(){
		if($_POST['customer_profile_save'] == 'Y'){
			$basic_info = array();
			$customer = $_POST['customer'];
			$blacklist = $_POST['checkbox'];
			
			$basic_info['username'] = $customer['username'];
			$basic_info['email'] = $customer['email'];
			$basic_info['ip'] = $customer['ip'];
			$basic_info['status'] = $customer['status'];

					
			//updating the database
			$db = self::get_db_instace();
			$customer_id = $db->insert_customer($basic_info);
			
			if($customer_id){
				if($_POST['order_id'] > 0){
					update_post_meta($_POST['order_id'], '_verified_customer_id', $customer_id);
					$db->add_order_for_a_customer($customer_id, 'order_id', $_POST['order_id']);
				}				
				
				$db->update_customer_meta($customer_id, 'timezone', $customer['timezone']);
				$db->update_customer_meta($customer_id, 'city', $customer['city']);
				$db->update_customer_meta($customer_id, 'country', $customer['country']);
				
				$db->update_customer_meta($customer_id, 'note', $_POST['customer_additional_note']);
				$db->update_customer_meta($customer_id, 'attachment', $_POST['attachment']);
				$db->update_customer_meta($customer_id, 'name', $customer['name']);
				
				//ip ranges saving
				if(!empty($_POST['ip_range'])){
					if(ip2long($_POST['ip_range']['start']) && ip2long($_POST['ip_range']['end'])){
						$db->save_ip_ranges($customer_id, $_POST['ip_range']['start'], $_POST['ip_range']['end']);
					}
				}
				else{
					$db->remove_ip_ranges($customer_id);
				}
				
				
				//now fraud detection
				$blacklist_keys = self::$suspecious_list;
				
				foreach($blacklist_keys as $k){
					if(isset($blacklist[$k])){
						$db->save_detected_frauds($customer_id, $k, $customer[$k]);
					}
					else{
						$db->remove_from_black_list($customer_id, $k, $customer[$k]);
					}
				}
				
				$message = 1;
			}
			else{
				$customer_id = 0;
				$message = 2;
			}
			
			
			$url = admin_url(sprintf('admin.php?page=anti-fraud-management&customer_id=%s&message=%s', $customer_id, $message));
			return self::do_redirect($url);
		}
	}
	
	
	static function get_customer_info_by_order($order_id){
		$customer = array();
		$order = new WC_Order($order_id);
		
		//wp user checking
		$user_id = get_post_meta($order->id, '_customer_user', true);
		$user = get_user_by('id', $user_id);
		
		//var_dump($order);
		$customer['name'] = array('title'=>'Billing Name', 'value'=>$order->billing_first_name . ' ' . $order->billing_last_name);
		$customer['username'] = array('title' =>'Username', 'value'=>empty($user) ? 'Non User' : $user->data->user_login);
		$customer['email'] = array('title'=>'Billing Email', 'value'=>$order->billing_email);
		
		
		$customer['payment_method'] = array('title'=>'Payment Method', 'value'=>$order->payment_method);
		$customer['ip'] = array('title'=>'IP Address', 'value'=>get_post_meta($order->id, '_customer_ip_address', true));
		$customer['city'] = array('title'=>'City (traced by IP)', 'value'=>get_post_meta($order->id, '_ip_city', true));
		$customer['country'] = array('title'=>'Country (traced by IP)', 'value'=>get_post_meta($order->id, '_ip_country', true));
		$customer['timezone'] = array('title' => 'Timezone (traced by IP)', 'value'=>get_post_meta($order->id, '_ip_time_zone', true));
			
		return $customer;
	}
	
	
	
	
	/**
	 * Update Fraud lists
	 * */
	static function update_fraud_lists($by){
		$db = self::get_db_instace();
		$lists = $db->get_blacklists_by($by);
		if($lists){
			foreach($lists as $list){
				if(isset($_POST[$by][$list->id])){
					$db->update_blacklist_status('id', $list->id, 1);
				}
				else{
					$db->update_blacklist_status('id', $list->id, 2);
				}
			}
		}
		
	}
	
	
	/**
	 * processing an order
	 * @hook woocommerce_checkout_process
	 * */
	static function verify_customer(){
		global $woocommerce;
		$billing_email = $_POST['billing_email'];
		$db = self::get_db_instace();
		
		//trying to get customer by billing email
		$customer = $db->get_customer_by('email', $billing_email);
		
		//trying to get customer by current loggedin user
		if(empty($customer) && is_user_logged_in()){
			$current_user = wp_get_current_user();
			
			//trying to get customer by current user username
			$customer = $db->get_customer_by('username', $current_user->user_login);
			
			//trying to get customer by current user email
			if(empty($customer)){
				$customer = $db->get_customer_by('email', $current_user->user_email);
			}
		}
		
		if($customer){		
			if($db->is_black_listed($customer->ID, 'email', $billing_email) || $db->is_black_listed($customer->ID, 'username', $customer->username)){
				$message = self::woocommerce_hard_blacklist_message();
				if(!$message) $message = self::$default_message;
				
				$woocommerce->add_error( '<strong>Sorry! </strong> ' . __( $message, 'woocommerce' ) );
			}					
		}
		
					
	}
	
	
	
	/**
	 * do a redirect with the given url
	 * */
	static function do_redirect($url){
		if($url){
			if(!function_exists('wp_redirect')){
				include ABSPATH . '/wp-includes/pluggable.php';
			}
			
			wp_redirect($url);
			exit;
		}
	}
	
	
	
	/**
	 *tab functionality 
	 * */
	static function get_tab_url($tab){
		return sprintf(admin_url('admin.php?page=anti-fraud-management&tab=%s'), $tab['slug']);	
	}
	
	
	
	/**
	 * load the appropriate tab
	 * */
	static function get_appropirate_tab(){
		if(isset($_GET['tab']) && !empty($_GET['tab'])){
			$tab = $_GET['tab'];
		}
		else{
			$tab = 'verified';
		}

		return self::get_file_dir('/templates/admin/tabs/' . $tab . '.php');
	}

	
	/**
	 * Load and get the list table
	 * */
	static function get_list_table($type = ''){
		if(!class_exists('CustomersListTable')){
			include self::get_file_dir('/classes/class.list-tabe.php');
		}

		return new CustomersListTable($type);
	}
	
	
	
	/**
	 * Making form friendly structure with array
	 * */
	static function make_form_layout($info = array()){
		$form_layout = array();
		
		foreach($info as $key => $value){
			switch($key){
				case 'username':
					$form_layout[$key] = array('title' => 'Username', 'value' => $value);
					break;
				case 'email':
					$form_layout[$key] = array('title' => 'Email', 'value' => $value);
					break;
				case 'ip':
					$form_layout[$key] = array('title' => 'IP Address', 'value' => $value);
					break;
				case 'city':
					$form_layout[$key] = array('title' => 'City (Traced by Ip)', 'value' => $value);
					break;
				case 'country':
					$form_layout[$key] = array('title' => 'Country (Traced by Ip)', 'value' => $value);
					break;
				case 'timezone':
					$form_layout[$key] = array('title' => 'Timezone (Traced by Ip)', 'value' => $value);
					break;
				case 'payment_method':
					$form_layout[$key] = array('title' => 'Payment Method', 'value' => $value);
					break;
				default:
					$form_layout[$key] = array('title' => ucfirst($key), 'value' => $value);
			}
		}
		
		return $form_layout;
	}
	
	
	/**
	 * List table form handler
	 * */
	static function list_table_form_handler(){
		if(($_REQUEST['fraud_management_form'] == 'Y') || ($_REQUEST['page'] == 'anti-fraud-management' && $_REQUEST['customer_id'] > 0 && $_REQUEST['action'] == 'delete')){
			$sendback = remove_query_arg( array('deleted', 'customer_id', 'fraud_management_form'), wp_get_referer() );
		
		
			if(!$sendback){
				$sendback = admin_url('admin.php?page=anti-fraud-management');
			}
			
			$tab = $_REQUEST['tab'];
			$type = self::get_type($tab);		
			
			$wp_list_table = self::get_list_table($type);
			$doaction = $wp_list_table->current_action();
			$pagenum = $wp_list_table->get_pagenum();
				
			$sendback = add_query_arg( 'paged', $pagenum, $sendback );
			
			if($doaction == 'delete'){
				$deleted = $wp_list_table->handle_bulk_action();
				$sendback = add_query_arg('deleted', $deleted, $sendback);
			}
			
			$sendback = remove_query_arg( array('action', 'action2', '_wp_http_referer', '_wpnonce'), $sendback );
			if(!empty($_REQUEST['s'])){
				$sendback = add_query_arg( 's', urlencode($_REQUEST['s']), $sendback );
			}
			
			return self::do_redirect($sendback);
		
		}
		
	}
	
	//get the type
	static function get_type($tab){
		switch($tab){
			case 'blacklisted':
				return 'hard';
				break;
			case 'soft-blacklisted':
				return 'soft';
				break;
			default:
				return 'ok';
				break;
		}		
	}
	
	
	//get some orders
	static function get_orders($customer_id){
		$order_args = array(
				'post_type' => 'shop_order',
				'posts_per_page' => 10,
				'meta_query' => array(
					array(
						'key' => '_verified_customer_id',
						'value' => $customer_id,
						'compare' => '='		
					)
				),
				'fields' => 'ids',
				'orderby' => 'date',
				'order' => 'desc'
				
		);
		
		$query = new WP_Query( $order_args );		
		wp_reset_query();		
		return $query->posts;
	}
	
	
	//settings tab
	static function add_new_settings_tab($tabs){
		$tabs['anti_fraud'] = __('Anti Fraud Notices', 'woocommerce');
		return $tabs;
	}
	
	//populate new settings tab
	static function populate_new_settings_tab(){
		include self::get_file_dir('/templates/settings-tab/anti-fraud-notices.php');
	}
	
	//save new settings tab
	static function save_new_settings_tab(){
				
		if(isset($_POST['woocommerce_hard_blacklist_message'])){
			update_option('woocommerce_hard_blacklist_message', $_POST['woocommerce_hard_blacklist_message']);
		}		
	}
	
	//get the message for hard blaclisted customers
	static function woocommerce_hard_blacklist_message(){
		return get_option('woocommerce_hard_blacklist_message');
	}
	
}